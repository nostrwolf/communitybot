import 'package:dartcommunitybot/dartcommunitybot.dart' as community;
import 'package:test/test.dart';

import '../bin/dartcommunitybot.dart';

void main() {
  test('bannedusers', () {
    Set<String> bannedUsers = <String>{"jsonborn"};

    var allowed = allowedToPost(bannedUsers, <String>{}, <String>[], "", "");
    expect(allowed, true);

    allowed =
        allowedToPost(bannedUsers, <String>{}, <String>[], "jsonborn", "");

    expect(allowed, false);
  });

  test('bannedwords', () {
    List<String> bannedWords = <String>["kill"];

    var allowed = allowedToPost(<String>{}, <String>{}, bannedWords, "", "");
    expect(allowed, true);

    allowed =
        allowedToPost(<String>{}, <String>{}, bannedWords, "jsonborn", "kill");

    expect(allowed, false);
  });
}
