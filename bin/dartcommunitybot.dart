import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:nostr/nostr.dart';
import 'package:test/test.dart';

String readConfigAsString() {
  print("reading in file");
  var configdir = Platform.environment["CONFIGDIR"] ?? "";
  var contents = File(configdir).readAsStringSync();
  return contents;
}

Set<String> getBannedUsers() {
  var config = readConfigAsString();
  var tagsJson = jsonDecode(config)['bannedusers'];
  List<String> tags = tagsJson != null
      ? List.from(tagsJson)
      : List<String>.empty(growable: true);

  Set<String> returnValue = {};

  for (int i = 0; i < tags.length; i++) {
    returnValue.add(Nip19.decodePubkey(tags[i]));
  }
  return returnValue;
}

List<String> getBannedWords() {
  var config = readConfigAsString();
  var tagsJson = jsonDecode(config)['bannedwords'];
  List<String> tags = tagsJson != null
      ? List.from(tagsJson)
      : List<String>.empty(growable: true);

  return tags;
}

Set<String> getAllowedUsers() {
  var config = readConfigAsString();
  var tagsJson = jsonDecode(config)['allowedusers'];
  List<String> tags = tagsJson != null
      ? List.from(tagsJson)
      : List<String>.empty(growable: true);

  Set<String> returnValue = {};

  for (int i = 0; i < tags.length; i++) {
    returnValue.add(Nip19.decodePubkey(tags[i]));
  }
  return returnValue;
}

Event createApprovedEvent(
    Event e, String community, String relay, String privateKey) {
  final jsonEncoder = JsonEncoder();
  List<List<String>> tags = [];

  List<String> tag1 = ["a", "34550:$community", relay];
  List<String> tag2 = ["e", e.id, relay];
  List<String> tag3 = ["p", e.pubkey, relay];
  List<String> tag4 = ["k", "1"];

  tags.add(tag1);
  tags.add(tag2);
  tags.add(tag3);
  tags.add(tag4);
  var content = jsonEncoder.convert(e);
  Event retVal = Event.from(
    kind: 4550,
    tags: tags,
    content: content,
    privkey: privateKey, // DO NOT REUSE THIS PRIVATE KEY
  );

  return retVal;
}

bool allowedToPost(Set<String> bannedUsers, Set<String> allowedUsers,
    List<String> bannedWords, String pubkey, String content) {
  // Manually approve these

  for (var i = 0; i < bannedWords.length; i++) {
    if (content.contains(bannedWords[i])) {
      print("Contains banned word so not auto approving");
      return false;
    }
  }

  if (bannedUsers.contains(pubkey)) {
    print("banned user:${pubkey} attempted to post. Not approving");
    return false;
  }

  if (allowedUsers.length == 0 || allowedUsers.contains(pubkey)) {
    // If no allwedUsers accept all
    return true;
  }

  print("user:${pubkey} not authorizing");
  return false;
}

void queryRelay(Set<String> bannedUsers, Set<String> allowedUsers,
    List<String> bannedWords) async {
  print("starting query");

  print(DateTime.now().millisecondsSinceEpoch);
  var relay = Platform.environment["RELAY"] ?? "";

  int secondsSinceEpoch =
      DateTime.now().millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond;

  var intervalPeriod = Platform.environment["QUERYPERIODINMINUTES"] ?? "";
  int minutes = 5;
  try {
    minutes = int.parse(intervalPeriod);
  } catch (e) {
    print(e);
  }

  int intervalPeriodInSeconds = secondsSinceEpoch - (minutes * 60);
  var communityName = Platform.environment["COMMUNITYNAME"] ?? "";
  var communityCreator = Platform.environment["COMMUNITYCREATORNPUB"] ?? "";
  print("creator:" + communityCreator);
  var community = "${Nip19.decodePubkey(communityCreator)}:$communityName";
  var communitySearch = "34550:$community";
// Create a subscription message request with one or many filters
  Request requestWithFilter = Request(generate64RandomHexChars(), [
    Filter(
      kinds: [
        1,
        4549,
        4550,
      ],
      a: [communitySearch],
      since: intervalPeriodInSeconds,
      limit: 10000,
    )
  ]);

  // Connecting to a nostr relay using websocket
  WebSocket webSocket = await WebSocket.connect(
    relay, // Fill in with any relay
  );

  // Send a request message to the WebSocket server
  webSocket.add(requestWithFilter.serialize());

  // Listen for events from the WebSocket server
  var approvedRequest = List<Event>.empty(growable: true);
  var maybeApprovedRequest = List<Event>.empty(growable: true);

  print("Searching for community:$communitySearch");
  await Future.delayed(Duration(seconds: 1));
  webSocket.listen((event) {
    Event event2;
    Message message;

    try {
      message = Message.deserialize(event);
      try {
        event2 = message.message;
        for (var i = 0; i < event2.tags.length; i++) {
          var outer = event2.tags[i];
          for (var j = 0; j < outer.length; j++) {
            var inner = outer[j];
            if (inner == communitySearch) {
              if (event2.kind == 4550 ||
                  event2.kind == 1 ||
                  event2.kind == 4549) {
                // zapddit still uses kind 4549
                if (event2.kind == 4550) {
                  approvedRequest
                      .add(event2); // Pick out already approved request
                } else if (allowedToPost(bannedUsers, allowedUsers, bannedWords,
                    event2.pubkey, event2.content)) {
                  maybeApprovedRequest.add(event2);
                  // Get recent post to compare against request already approved
                }
              }
            }
          }
        }
      } catch (f) {} // Just do nothing
    } catch (e) {} //just do nothing
  });

  await webSocket.close().then((value) => null);
  await Future.delayed(Duration(seconds: 1));
  print("length approved:${approvedRequest.length}");
  print("length unapproved:${maybeApprovedRequest.length}");

  Set<String> approvedItems = {};
  List<Event> NeedsApproving = List.empty(growable: true);
  for (var i = 0; i < approvedRequest.length; i++) {
    var item = json.decode(approvedRequest[i].content);
    try {
      Event deserialzed = Event.fromJson(item);
      approvedItems.add(deserialzed.id);
    } catch (e) {
      print("malformed content. Should be valid json. It's not");
      print(e);
    }
  }

  print("printing known approval stuff out!");
  for (var i = 0; i < maybeApprovedRequest.length; i++) {
    if (approvedItems.contains(maybeApprovedRequest[i].id)) {
      print("Already approved ignore");
    } else {
      NeedsApproving.add(maybeApprovedRequest[i]);
    }
  }

  print("Need to approve:${NeedsApproving.length} items");

  // Connecting to a nostr relay using websocket
  webSocket = await WebSocket.connect(
    relay, // or any nostr relay
  );

  var pknsec = Platform.environment["PRIVATEKEY"] ?? "";
  var pk = Nip19.decodePrivkey(pknsec);

  for (var i = 0; i < NeedsApproving.length; i++) {
    var event = createApprovedEvent(NeedsApproving[i], community, relay, pk);
    webSocket.add(event.serialize());
  }
  await Future.delayed(Duration(seconds: 1));
  webSocket.listen((event) {
    print('Received event: $event');
  });

  // Close the WebSocket connection
  await webSocket.close();
}

void main() async {
  var bannedUsers = getBannedUsers();
  var allowedUsers = getAllowedUsers();
  var bannedWords = getBannedWords();
  queryRelay(bannedUsers, allowedUsers, bannedWords); //Do a query on start up
  var secondStrings = Platform.environment["QUERYINTERVALINSECONDS"] ?? "";
  int s = 60;
  try {
    s = int.parse(secondStrings);
  } catch (e) {
    print(e);
  }
  try {
    //Query after set interval and continue to query
    var period = Duration(seconds: s);
    Timer.periodic(period,
        (Timer t) => queryRelay(bannedUsers, allowedUsers, bannedWords));
  } catch (e) {
    print(e);
  }
}
