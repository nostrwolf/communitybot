# Nostr Community Bot

Approves post automatically if user is not in block list.

## Why?

Currently all post to Nostr communities need to be approved before being seen in community UI's. 
This makes it feel less fun than reddit or other community sites. 

This program allows you to specify a relay, a community, and a moderator's private key to auto approve post over a interval on that relay.

## How to use

This service is designed to work in docker compose. Fill in the following environment variables in the docker-compose.yml file

```
    environment:
         - "RELAY=wss:/YOURRELAYURL"
         - "PRIVATEKEY=YOUR PRIVATENSEC KEY, OR THE NSEC OF A MODERATOR OF THE COMMUNITY" # Careful with this one!
         - "COMMUNITYNAME=NAME OF COMMUNITY"
         - "COMMUNITYCREATORNPUB=npub of the user who created the community. It's used in its name internally" # 
         - "QUERYINTERVALINSECONDS=60" # how often to query
         - "QUERYPERIODINMINUTES=15" # how far to query
         - "CONFIGDIR=/app/config.json" # config file location with users you want to block. Example is given with an annoying bot.
```

COMMUNITYCREATORNPUB is the community creator's npub. The reason is because internally nostr
names it's communities like this,

"34550:CREATORSPUBLICKEYINHEX:COMMUNITYNAME"

This program takes care of converting from npub to hex so you can just copy and paste the npub into the environment variable.

QUERYINTERVALINSECONDS is how often to query

QUERYPERIODINMINUTES is how long ago to pull events from.

It's currently set to query every 1 minute and it queries back 15 minutes. So if something is missed it should pick it up the next time.


The config file has a list of banned users you can keep from posting. I gave an example bot whom is really annoying.


```
{
    "bannedusers":["npub13wfgha67mdxall3gqp2hlln7tc4s03w4zqhe05v4t7fptpvnsgqs0z4fun"]
}
```

It has allowedusers which if used will only auto approve post from those users. Leave empty to approve all users not in bannedusers list.

It has bannewords list and a default example is is given of "kill". Any post made with these words will have to be manually approved. 
  

To add more relays and communities you can copy the whole service stanza and paste it again with a different community or relay. 

Once you've filled in those values just start it up with

```
docker compose up -d
```

And you are good to go! No open ports needed. Runs in a scratch container. 

## future features to be added.

1. Query more than one relay per service. 
2. What else? Suggest?

## Liscence 

lgpl-2.1 

If you pass it around share your code.



