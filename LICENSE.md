Nostr Moderator bot
Copyright (C) 2023  nostrwolf.com

Nostr npub for contact: npub1wg6u23gjc2y33fxukr284fy8p8lw7hztu30gtat89nwy4x3j4pcsh0xxwc

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; 
version 2.1

Some libraries used are version 3 of gpl.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

